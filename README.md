# Go React website

Basic setup for typescript based react website, that is served using go https server.

**IMPORTANT NOTE!** certificates under this repository should not be used outside 
testing environment as they are not secure!! if you need real certificates, check Let's Encrypt
and Certbot for automatically renewing certificates: https://certbot.eff.org/instructions

## License

MIT granted on 26th of September 2021.

## Stack version information

|Name|Version|
|:-:|:-:|
|Go|1.16.4|
|Node.js|14.17.4|
|npm|6.14.14|

## Quickstart.

### Build website

    npm run build

this command builds the website to project root 'dist' folder.

#### Production mode

webpack.conf.js file has a boolean flag for production.

### Serve website

Create .env file. Check required keys here: https://gitlab.com/raojala/go-react-website/-/blob/main/src/go/env_wrapper/environment-settings.go

I.e.

    WEBSITE_BUILD_PATH="./dist"
    SERVER_CERTIFICATE_CRT="server.crt"
    SERVER_CERTIFICATE_KEY="server.key"
    WEBSITE_IP_ADDRESS="127.0.0.1"
    WEBSITE_PORT="443"
    WEBSITE_STATIC_FILE_PATH="dist/"
    MYSQL_USER="root"
    MYSQL_PASSWORD="salasana"
    MYSQL_DATABASE_ADDRESS="192.168.0.102"
    MYSQL_PORT="3306"
    MYSQL_DATABASE="basic_website"
    MYSQL_PROTOCOL="tcp"
    MYSQL_MAX_CONNECTIONS="10"
    MYSQL_MAX_IDLE_CONNECTIONS="10"
    MYSQL_MAX_CONNECTION_LIFETIME_SECONDS="300"
    COOKIE_LIFETIME_SECONDS="1800"

NOTE: remember to document your env variables somewhere other than version control

Then run following command:

    go run .

This starts an HTTPS server using server.crt and server.key certificate files at project root, then serves the files from 'dist' folder.

### Folder structure

    /dist                                   # website is built and served from here
    /project-assets                         # Files to to use default project with. Mainly the database model that default database code refers to. Said model is made to be expandable and is depended on by some go packages made here.
    /src                                    # all code files
        /assets                             # this folder holds assets like images and/or audio
            favicon.svg                     # browser favicon copied over in build
        /go                                 # holds all go packages made for this project
            /dal                            # database code used to access mysql server crud operations (with example database code)
            /env_wrapper                    # wrapper that holds environment variables.
            /log_wrapper                    # log wrapper that prepends log level to message and inserts it to database. depends on dal package systemlog.go.
            /routes                         # holds home and API routes for the webserver. depends on /dal, /env_wrapper, /log_wrapper, /session. Moved out from root to make root clearer  
            /session                        # tracks the sessions and logged in users inside the webserver.
        /html                               # html-template that is used to build index.html with bundles   
            index.html                      # basic html5 html template file
        /typescript                         # react website and components in typescript
            /api_calls                      # api calls
            /components 
                /UICompositionComponent     # simple components that make compositions of other components to form new wholes
                /UIControlComponents        # can't remember why I wanted this here
                /UIVisualComponents         # simple react wrapper components for styling, such as row or column.
            /forms                          # simple reusable forms, such as login form or register form
            /packet_definitions             # files here define packets used to send/receive data between front- and backend
            /views                          # Small composition react components that form the "look" of the site.
            context.tsx                     # front send session control and other possible global settings you don't wish to propdrill
            defaults.scss                   # some default styles across the site
            defaults.tsx                    # handles setting default settings, like axios default headers
            main.tsx                        # main typescript file that hooks to index.html root div
    .env                                    # your environment variables. DO NOT STORE YOUR SECRETS IN VERSION CONTROL
    .eslintrc.json                          # ESLint settings. ESLint will format code based on these settings and helps you keep the code base clean
    .gitignore                              # .gitignore file that excludes files in version control
    go.mod                                  # file that holds go dependencies and module information
    api.go                                  # example API route and function that returns hello world to react
    main.go                                 # Go main file that holds go entrypoint.
    package.json                            # NPM file that holds node dependencies + other useful information. generated using npm init
    package-lock.json                       # generated file when package.json file changes
    README.MD                               # this file
    server.crt                              # server certification file used for testing and debugging. DO NOT USE OUTSID   E TESTING
    server.key                              # server key certification file used for testing and debugging. DO NOT USEE OUTSI   DE TESTING
    tsconfig.json                           # typescript settings file, genereated using tsc init
    webpack.conf.js                         # webpack configuration file