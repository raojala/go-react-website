const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// VARIABLES
const production = false;
const outputPath = './dist';
const assetPath = '/dist';
const entryFile = './src/typescript/main.tsx';
const indexTemplatePath = './src/html/index.html';
const faviconPath = './src/assets/favicon.svg';

module.exports = {
	watch: !production,
	mode: !production ? 'development' : 'production',
	devtool: !production ? 'inline-source-map' : false,
	entry: entryFile,
	output: {
		filename: '[name].js?[contenthash]',
		path: path.resolve(__dirname, outputPath),
		clean: true,
		publicPath: outputPath+'/'
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css?[contenthash]',
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: indexTemplatePath,
			favicon: faviconPath,
			publicPath: assetPath+'/'
		})
	],
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
				exclude: /node_modules/,
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				type: 'asset/resource',
			}
		]
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
	},
	optimization: {
		splitChunks: {
			chunks: 'all',
		},
	},
};