package session

import (
	"crypto/rand"
	"errors"
	"math/big"
	"net/http"
	"time"

	"golang.org/x/crypto/argon2"
)

var SessionStore map[string]*Session

type Session struct {
	ID                string
	IsLoggedIn        bool
	UserID            string
	Expires           time.Time
	ExpirationSeconds int
	CSRFToken
}

type CSRFToken struct {
	CSRF string
}

func InitSessionManagement() {
	SessionStore = make(map[string]*Session)
}

func NewSessionCookie(rw http.ResponseWriter, expires int) (*http.Cookie, error) {

	var err error

	s := &Session{}

	s.ID, err = CryptoString(32)
	if err != nil {
		return nil, err
	}

	s.CSRF, err = CryptoString(64)
	if err != nil {
		return nil, err
	}

	s.Expires = time.Now().Add(time.Duration(expires) * time.Second)
	s.ExpirationSeconds = expires
	SessionStore[s.ID] = s

	cookie := GetNewSecureCookie("session", s.ID, s.Expires)
	http.SetCookie(rw, cookie)
	return cookie, nil
}

func AddSessionLifetime(rw http.ResponseWriter, sess *Session) {
	SessionStore[sess.ID].Expires = time.Now().Add(time.Duration(sess.ExpirationSeconds) * time.Second)
}

func RemakeSession(id, csrf string, expires int) {

	deleteSession(id)

	s := &Session{}
	s.ID = id
	s.CSRF = csrf
	// we don't actually care how old the cookie is.
	// if backend session is expired the cookie can be brand spanking new
	// and we still remake it
	s.Expires = time.Now().Add(time.Duration(expires) * time.Second)
	s.ExpirationSeconds = expires
	SessionStore[s.ID] = s
}

func GetSession(sessionId string) (*Session, error) {
	session, ok := SessionStore[sessionId]
	if !ok {
		return nil, errors.New("no session found!")
	}

	if time.Now().After(session.Expires) {
		delete(SessionStore, sessionId)
		return nil, errors.New("session expired!")
	}

	// valid token, refresh session
	session.Expires = time.Now().Add(time.Duration(session.ExpirationSeconds) * time.Second)

	return session, nil
}

func deleteSession(sessionId string) error {
	_, ok := SessionStore[sessionId]
	if !ok {
		return errors.New("no session found!")
	}

	delete(SessionStore, sessionId)
	return nil
}

func SetSessionLogin(session *Session) error {
	session, ok := SessionStore[session.ID]
	if !ok {
		return errors.New("no session found!")
	}

	SessionStore[session.ID] = session
	return nil
}

func HashPassword(password string, time, mem uint32) (string, []byte, error) {
	salt, err := CryptoString(128)
	if err != nil {
		return "", nil, err
	}

	key := argon2.IDKey([]byte(password), []byte(salt), time, mem, 4, 256)
	return salt, key, nil
}

func CryptoString(n int) (string, error) {
	// https://gist.github.com/dopey/c69559607800d2f2f90b1b1ed4e550fb
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	ret := make([]byte, n)
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			return "", err
		}
		ret[i] = letters[num.Int64()]
	}

	return string(ret), nil
}

func GetNewSecureCookie(name, value string, expires time.Time) *http.Cookie {
	c := &http.Cookie{}
	c.HttpOnly = true
	c.SameSite = http.SameSiteStrictMode
	c.Secure = true
	c.Name = name
	c.Value = value
	c.Expires = expires

	return c
}

func IsCookieSecure(cookie *http.Cookie) bool {
	if !cookie.HttpOnly || cookie.SameSite != http.SameSiteStrictMode || !cookie.Secure {
		return false
	}
	return false
}

func CheckPassword(password, salt string, time, mem uint32) []byte {
	key := argon2.IDKey([]byte(password), []byte(salt), time, mem, 4, 256)
	return key
}
