package env_wrapper

import (
	"errors"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var EnvSettings *EnvironmentSettings

type EnvironmentSettings struct {
	WebsiteBuildPath                  string
	ServerCertificateCrt              string
	ServerCertificateKey              string
	WebsiteIPAddress                  string
	WebsitePort                       string
	WebsiteStaticFilesPath            string
	WebsiteName                       string
	MySQLUser                         string
	MySQLPassword                     string
	MySQLDatabaseAddress              string
	MySQLPort                         string
	MySQLDatabase                     string
	MySQLProtocol                     string
	MySQLMaxConnections               int
	MySQLMaxIdleConnections           int
	MySQLMaxConnectionLifetimeSeconds int
	CookieLifetimeSeconds             int
}

func (E *EnvironmentSettings) Validate() error {

	var emptyString string
	var emptyInt int

	if _, err := os.Stat(E.WebsiteBuildPath); os.IsNotExist(err) {
		return errors.New("Build path: '" + E.WebsiteBuildPath + "' does not exist.")
	}
	if E.WebsiteBuildPath == emptyString {
		return errors.New("Missing setting: WEBSITE_BUILD_PATH")
	}
	if E.ServerCertificateCrt == emptyString {
		return errors.New("Missing setting: SERVER_CERTIFICATE_CRT")
	}
	if E.ServerCertificateKey == emptyString {
		return errors.New("Missing setting: SERVER_CERTIFICATE_KEY")
	}
	if E.WebsiteIPAddress == emptyString {
		return errors.New("Missing setting: WEBSITE_IP_ADDRESS, listening all interfaces")
	}
	if E.WebsitePort == emptyString {
		return errors.New("Missing setting: WEBSITE_PORT")
	}
	if E.MySQLUser == emptyString {
		return errors.New("Missing setting: MYSQL_USER")
	}
	if E.MySQLPassword == emptyString {
		return errors.New("Missing setting: MYSQL_PASSWORD")
	}
	if E.MySQLDatabaseAddress == emptyString {
		return errors.New("Missing setting: MYSQL_DATABASE_ADDRESS")
	}
	if E.MySQLPort == emptyString {
		return errors.New("Missing setting: MYSQL_PORT")
	}
	if E.MySQLDatabase == emptyString {
		return errors.New("Missing setting: MYSQL_DATABASE")
	}
	if E.MySQLProtocol == emptyString {
		return errors.New("Missing setting: MYSQL_PROTOCOL")
	}
	if E.MySQLMaxConnections == emptyInt {
		return errors.New("Missing setting: MYSQL_MAX_CONNECTIONS, setting needs to be higher than 0")
	}
	if E.MySQLMaxIdleConnections == emptyInt {
		return errors.New("MYSQL_MAX_IDLE_CONNECTIONS is set to zero, no idle connections are allowed")
	}
	if E.MySQLMaxIdleConnections == emptyInt {
		return errors.New("MYSQL_MAX_CONNECTION_LIFETIME_SECONDS is set to zero, connections will not timeout")
	}
	if E.CookieLifetimeSeconds == emptyInt {
		return errors.New("COOKIE_LIFETIME_SECONDS is set to zero, defaulting to 1 hour")
	}

	if _, err := os.Stat(E.ServerCertificateCrt); os.IsNotExist(err) {
		return errors.New(E.ServerCertificateCrt + "' does not exist.")
	}
	if _, err := os.Stat(E.ServerCertificateKey); os.IsNotExist(err) {
		return errors.New(E.ServerCertificateKey + "' does not exist.")
	}
	if _, err := os.Stat(E.WebsiteStaticFilesPath); os.IsNotExist(err) {
		return errors.New(E.WebsiteStaticFilesPath + "' does not exist.")
	}
	if _, err := os.Stat(E.WebsiteName); os.IsNotExist(err) {
		return errors.New(E.WebsiteName + "' does not exist.")
	}

	return nil
}

func LoadSettings() error {
	if EnvSettings != nil {
		return errors.New("environment settings already loaded, tried to load twice")
	}

	if err := godotenv.Load(); err != nil {
		return errors.New("cannot load .env file. \nRead Quickstart in README.MD, you can find basic .env example there")
	}
	var err error
	EnvSettings = &EnvironmentSettings{}
	EnvSettings.WebsiteBuildPath = os.Getenv("WEBSITE_BUILD_PATH")
	EnvSettings.ServerCertificateCrt = os.Getenv("SERVER_CERTIFICATE_CRT")
	EnvSettings.ServerCertificateKey = os.Getenv("SERVER_CERTIFICATE_KEY")
	EnvSettings.WebsiteIPAddress = os.Getenv("WEBSITE_IP_ADDRESS")
	EnvSettings.WebsitePort = os.Getenv("WEBSITE_PORT")
	EnvSettings.WebsiteStaticFilesPath = os.Getenv("WEBSITE_STATIC_FILE_PATH")
	EnvSettings.WebsiteName = os.Getenv("WEBSITE_NAME")
	EnvSettings.MySQLUser = os.Getenv("MYSQL_USER")
	EnvSettings.MySQLPassword = os.Getenv("MYSQL_PASSWORD")
	EnvSettings.MySQLDatabaseAddress = os.Getenv("MYSQL_DATABASE_ADDRESS")
	EnvSettings.MySQLPort = os.Getenv("MYSQL_PORT")
	EnvSettings.MySQLDatabase = os.Getenv("MYSQL_DATABASE")
	EnvSettings.MySQLProtocol = os.Getenv("MYSQL_PROTOCOL")

	if EnvSettings.CookieLifetimeSeconds, err = strconv.Atoi(os.Getenv("COOKIE_LIFETIME_SECONDS")); err != nil {
		EnvSettings.CookieLifetimeSeconds = 3600
	}

	if EnvSettings.MySQLMaxConnections, err = strconv.Atoi(os.Getenv("MYSQL_MAX_CONNECTIONS")); err != nil {
		EnvSettings.MySQLMaxConnections = 0
	}

	if EnvSettings.MySQLMaxIdleConnections, err = strconv.Atoi(os.Getenv("MYSQL_MAX_IDLE_CONNECTIONS")); err != nil {
		EnvSettings.MySQLMaxIdleConnections = 0
	}

	if EnvSettings.MySQLMaxConnectionLifetimeSeconds, err = strconv.Atoi(os.Getenv("MYSQL_MAX_CONNECTION_LIFETIME_SECONDS")); err != nil {
		EnvSettings.MySQLMaxConnectionLifetimeSeconds = 0
	}

	return EnvSettings.Validate()
}
