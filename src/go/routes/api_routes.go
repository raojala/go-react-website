package routes

import (
	"net/http"
)

func SetupAPI() *http.ServeMux {

	apiMux := http.NewServeMux()

	// Require valid CSRF only
	apiMux.Handle("/api/checksessionuser", BasicApiSecurityHeaders(CheckSessionsUser))
	apiMux.Handle("/api/register", BasicApiSecurityHeaders(NewUser))
	apiMux.Handle("/api/login", BasicApiSecurityHeaders(LoginUser))

	// Require login
	apiMux.Handle("/api/logout", ApiRequireLogin(LogoutUser))

	return apiMux
}
