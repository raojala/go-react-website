package routes

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"https-react/src/go/dal"
	"https-react/src/go/log_wrapper"
	"https-react/src/go/session"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

func LoginUser(rw http.ResponseWriter, req *http.Request, currentSession *session.Session) {

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log_wrapper.SysLog.Error.Println(err)
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write(nil)
		return
	}

	credentials := &AxiosILoginPacket{}
	err = json.Unmarshal(body, credentials)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("failed to parse AxiosPacketWrapper", err)
		return
	}

	user := &dal.LoginCredential{}
	users, err := user.GetByEmail(credentials.Data.Email)
	if err != nil || len(users) == 0 {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("Could not find user", credentials.Data.Email, err)
		return
	}
	user = users[0]

	// 0 = algorithm
	// 1 = time multiplier
	// 2 = memory usage
	// 3 = salt
	// 4 = hash
	passwordParts := strings.Split(user.Password, "$")
	if len(passwordParts) < 5 {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println(errors.New("password hash is malformed, should be in five parts split with '$', but got less"))
		return
	}

	timeMultiplier, err := strconv.ParseUint(passwordParts[1], 10, 32)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("bad password hash, time multiplier parse failed", err)
		return
	}

	memoryUsage, err := strconv.ParseUint(passwordParts[2], 10, 32)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("bad password hash, memory usage parse failed", err)
		return
	}

	pwdHash := session.CheckPassword(credentials.Data.Password, passwordParts[3], uint32(timeMultiplier), uint32(memoryUsage))
	if base64.StdEncoding.EncodeToString(pwdHash) != passwordParts[4] {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("bad password or username", credentials.Data.Email)
		return
	}

	currentSession.IsLoggedIn = true
	currentSession.UserID = user.Id
	session.SetSessionLogin(currentSession)

	rw.Header().Add("Content-Type", "text/plain")
	rw.WriteHeader(http.StatusOK)
	rw.Write(nil)
}

func LogoutUser(rw http.ResponseWriter, req *http.Request, currentSession *session.Session) {

	sessionCookie, err := req.Cookie("session")
	if err != nil {
		http.Error(rw, "error logging you out, try refreshing page", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("invalid session", err)
		return
	}

	var sess *session.Session
	sess, err = session.GetSession(sessionCookie.Value)
	if err != nil {
		http.Error(rw, "error logging you out, try refreshing page", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println(err)
		return
	}

	session.RemakeSession(sess.ID, sess.CSRF, sess.ExpirationSeconds)

	rw.Header().Add("Content-Type", "text/plain")
	rw.WriteHeader(http.StatusOK)
	rw.Write(nil)
}
