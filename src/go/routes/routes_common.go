package routes

import (
	"https-react/src/go/dal"
	"https-react/src/go/log_wrapper"
	"https-react/src/go/session"
	"net/http"
	"net/mail"
)

func BasicApiSecurityHeaders(endpoint func(http.ResponseWriter, *http.Request, *session.Session)) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		reqCSRF := req.Header.Get("X-CSRF-TOKEN")
		sessionCookie, err := req.Cookie("session")
		if err != nil {
			http.Error(rw, "", http.StatusBadRequest)
			log_wrapper.SysLog.Error.Println(http.ErrNoCookie, err)
			return
		}

		currentSession, err := session.GetSession(sessionCookie.Value)
		if currentSession.CSRF != reqCSRF {
			http.Error(rw, "", http.StatusBadRequest)
			log_wrapper.SysLog.Error.Println("Bad CSRF token on API call", err)
			return
		}
		if err != nil {
			http.Error(rw, "", http.StatusBadRequest)
			log_wrapper.SysLog.Error.Println("Session not found", err)
		}

		endpoint(rw, req, currentSession)
	})
}

func ApiRequireLogin(endpoint func(http.ResponseWriter, *http.Request, *session.Session)) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		reqCSRF := req.Header.Get("X-CSRF-TOKEN")
		sessionCookie, err := req.Cookie("session")
		if err != nil {
			http.Error(rw, "", http.StatusBadRequest)
			log_wrapper.SysLog.Error.Println(http.ErrNoCookie, err)
			return
		}

		currentSession, err := session.GetSession(sessionCookie.Value)
		if err != nil || currentSession.CSRF != reqCSRF || currentSession.IsLoggedIn == false {
			http.Error(rw, "", http.StatusBadRequest)
			log_wrapper.SysLog.Error.Println("API call when not logged in", err)
			return
		}

		endpoint(rw, req, currentSession)
	})
}

func CheckSessionsUser(rw http.ResponseWriter, req *http.Request, currentSession *session.Session) {

	if currentSession.IsLoggedIn == false {
		http.Error(rw, "", http.StatusBadRequest)
		return
	}

	session.AddSessionLifetime(rw, currentSession)

	rw.Header().Add("Content-Type", "text/plain")
	rw.WriteHeader(http.StatusOK)
	rw.Write(nil)
}

func validMailAddress(address string) (string, bool) {
	addr, err := mail.ParseAddress(address)
	if err != nil {
		return "", false
	}
	return addr.Address, true
}

type AxiosILoginPacket struct {
	Data dal.LoginCredential
}
