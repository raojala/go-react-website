package routes

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"https-react/src/go/log_wrapper"
	"https-react/src/go/session"
	"io/ioutil"
	"net/http"

	passwordvalidator "github.com/wagslane/go-password-validator"
)

func NewUser(rw http.ResponseWriter, req *http.Request, currentSession *session.Session) {

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println(err)
		return
	}

	newUser := &AxiosILoginPacket{}
	err = json.Unmarshal(body, newUser)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("failed to parse AxiosILoginPacket", err)
		return
	}

	const minEntropyBits = 60
	if err := passwordvalidator.Validate(newUser.Data.Password, minEntropyBits); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("password strength bad", err)
		return
	}

	validMail, valid := validMailAddress(newUser.Data.Email)
	if !valid {
		http.Error(rw, "malformed email", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("malformed email", err)
		return
	}
	newUser.Data.Email = validMail

	var timeMultiplier uint32 = 3
	var memoryUsage uint32 = 32 * 1024
	salt, pwdHash, err := session.HashPassword(newUser.Data.Password, timeMultiplier, memoryUsage)
	if err != nil {
		log_wrapper.SysLog.Error.Println("error while hashing password", err)
	}

	databasePwd := "argon2id$" + fmt.Sprintf("%d", timeMultiplier) + "$" + fmt.Sprintf("%d", memoryUsage) + "$" + salt + "$" + base64.StdEncoding.EncodeToString(pwdHash)
	newUser.Data.Password = databasePwd

	err = newUser.Data.Insert()
	if err != nil {
		log_wrapper.SysLog.Error.Println(err)
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write(nil)
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write(nil)
}
