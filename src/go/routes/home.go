package routes

import (
	"errors"
	"html/template"
	"https-react/src/go/env_wrapper"
	"https-react/src/go/log_wrapper"
	"https-react/src/go/session"
	"net/http"
	"path/filepath"
	"strings"
)

func SetupWebserver(staticFilesPath, websiteBuildPath string) *http.ServeMux {
	fileServer := http.FileServer(http.Dir(websiteBuildPath))
	webserverMux := http.NewServeMux()
	webserverMux.Handle("/"+staticFilesPath, http.StripPrefix("/"+staticFilesPath, fileServer))
	webserverMux.HandleFunc("/", Home)
	return webserverMux
}

func Home(rw http.ResponseWriter, req *http.Request) {

	log_wrapper.SysLog.Debug.Println(req.URL)

	IndexTemplate := filepath.Join(strings.ReplaceAll("/"+env_wrapper.EnvSettings.WebsiteStaticFilesPath, "/", ""), "index.html")

	cookie, err := req.Cookie("session")
	if err != nil {
		cookie, err = session.NewSessionCookie(rw, env_wrapper.EnvSettings.CookieLifetimeSeconds)
		if err != nil {
			http.Error(rw, "", http.StatusBadRequest)
			log_wrapper.SysLog.Error.Println("could not create fresh cookie", err)
		}
	}

	currentSession, ok := session.SessionStore[cookie.Value]
	if !ok {
		log_wrapper.SysLog.Warn.Println(errors.New("invalid session id found"))
		cookie, err = session.NewSessionCookie(rw, env_wrapper.EnvSettings.CookieLifetimeSeconds)
		if err != nil {
			log_wrapper.SysLog.Error.Fatal(err)
		}
		currentSession, _ = session.SessionStore[cookie.Value]
	}

	tmpl, err := template.ParseFiles(IndexTemplate)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("could not parse index template", err)
	}

	session.AddSessionLifetime(rw, currentSession)

	viewMdl := &HomeViewMdl{}
	viewMdl.CSRF = session.SessionStore[cookie.Value].CSRF
	viewMdl.Title = env_wrapper.EnvSettings.WebsiteName

	err = tmpl.ExecuteTemplate(rw, "index", viewMdl)
	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
		log_wrapper.SysLog.Error.Println("could not execute index template", err)
	}

}

type HomeViewMdl struct {
	CSRF  string
	Title string
}
