package log_wrapper

import (
	"errors"
	"https-react/src/go/dal"
	"log"
	"time"
)

var SysLog *logWrapper

type logWrapper struct {
	Info  *log.Logger
	Debug *log.Logger
	Warn  *log.Logger
	Error *log.Logger

	*dal.SystemLog
}

func (L *logWrapper) Write(p []byte) (n int, err error) {
	L.SystemLog.TimeStamp = time.Now().UTC().Format("2006-01-02 15:04:05")
	L.SystemLog.Line = string(p)
	return len(p), L.Insert()
}

func NewLog(flags int) error {
	if SysLog != nil {
		return errors.New("Logger already exists")
	}

	SysLog = &logWrapper{}
	SysLog.SystemLog = &dal.SystemLog{}
	SysLog.Info = log.New(SysLog, "INFO: ", flags)
	SysLog.Debug = log.New(SysLog, "DEBUG: ", flags)
	SysLog.Warn = log.New(SysLog, "WARN: ", flags)
	SysLog.Error = log.New(SysLog, "ERROR: ", flags)

	return nil
}
