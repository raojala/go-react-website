import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { ILoginPacket } from '../packet_definitions/GOTypeInterfaces';

type FailCallbackFunc = (errorData : string) => void;  
type SuccessCallbackFunc = () => void;  

export const RegisterUser = (user: ILoginPacket, successCallback : SuccessCallbackFunc, failCallback : FailCallbackFunc) : void => {

	const extraOpts : AxiosRequestConfig = {
		data: user
	};

	axios.post('/api/register', extraOpts)
		.then(() => {
			successCallback();
		})
		.catch((error : AxiosError<string>) => {
			failCallback(error.response ? error.response.data : 'Unknown error');
		});
}; 

export const LoginUser = (user: ILoginPacket, successCallback : SuccessCallbackFunc, failCallback : FailCallbackFunc) : void => {

	const extraOpts : AxiosRequestConfig = {
		data: user,
	};

	axios.post('/api/login', extraOpts)
		.then(() => {
			successCallback();
		})
		.catch((error : AxiosError<string>) => {
			failCallback(error.response ? error.response.data : 'Unknown error');
		});
};

export const LogoutUser = (user: ILoginPacket, successCallback : SuccessCallbackFunc, failCallback : FailCallbackFunc) : void => {

	const extraOpts : AxiosRequestConfig = {
		data: user,
	};

	axios.post('/api/logout', extraOpts)
		.then(() => {
			successCallback();
		})
		.catch((error : AxiosError<string>) => {
			failCallback(error.response ? error.response.data : 'Unknown error');
		});
};

export const CheckLoginStatus = async (callback: (val: boolean) => void) : Promise<unknown> => {
	console.log('CheckLoginStatus');
	
	return axios.post('/api/checksessionuser')
		.then(() => {
			callback(true);
		})
		.catch(() => {
			callback(false);
		});
}; 