import React, { ChangeEvent } from 'react';
import { ILoginPacket } from '../packet_definitions/GOTypeInterfaces';
import { Collection } from '../components/UICompositionComponents/Collection/collection';
import { Column } from '../components/UIVisualComponents/Column/column';
import { RegisterUser } from '../api_calls/session_api_calls';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

export const RegisterUserForm = () : JSX.Element => {

	let _email = '';
	const EmailChanged = (e: ChangeEvent<HTMLInputElement>) : void => {
		_email = e.target.value;
	};

	let _password = '';
	const PasswordChanged = (e: ChangeEvent<HTMLInputElement>) : void => {
		_password = e.target.value;
	};

	const handleRegister = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		const user : ILoginPacket = ({
			Email: _email,
			Password: _password
		});
		RegisterUser(user, RegisterSuccess, RegisterFail);
	};

	const redirectHome = useNavigate();
	const RegisterSuccess = () : void => {
		toast.success('Register successful');
		redirectHome('/');
	};

	const RegisterFail = (data : string) => {
		toast.error(data ? data : 'register failed');
	};

	const FormContent = () : JSX.Element =>  
		<form onSubmit={handleRegister}>
			<Collection Title='Register'>
				<Column>
					<label htmlFor='email'>Email:</label>
					<input type='email' id='email' onChange={EmailChanged} required />

					<label htmlFor='fpassword'>Password:</label>
					<input type='password' id='fpassword' onChange={PasswordChanged} required />

					<input type='submit' value='Register' />

					<Link to='/login'>Login</Link>
				</Column>
			</Collection>
		</form>;

	return <FormContent />;
};

