import React, { useState, ChangeEvent } from 'react';
import { ILoginPacket } from '../packet_definitions/GOTypeInterfaces';
import { Collection } from '../components/UICompositionComponents/Collection/collection';
import { LoginUser, LogoutUser } from '../api_calls/session_api_calls';
import { Link } from 'react-router-dom';
import { Column } from '../components/UIVisualComponents/Column/column';
import { SessionContext } from '../context';
import { toast } from 'react-toastify';

export const LoginForm = () : JSX.Element => {
	
	const sessionContext = React.useContext(SessionContext);

	const [Email, SetEmail] = useState('');
	const EmailChanged = (e: ChangeEvent<HTMLInputElement>) : void => {
		SetEmail(e.target.value);
	};

	const [Password, SetPassword] = useState('');
	const PasswordChanged = (e: ChangeEvent<HTMLInputElement>) : void => {
		SetPassword(e.target.value);
	};

	const LoginSuccess = () => {
		toast.success('Login successful');
		sessionContext.SetLoggedInStatus(true);
	};
	
	const LoginFailed = (errorData : string) => {
		toast.error(errorData);
	};

	const handleLogin = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		const user : ILoginPacket = ({
			Email: Email,
			Password: Password
		});
		LoginUser(user, LoginSuccess, LoginFailed);
	};

	const LogoutSuccess = () => {
		toast.success('Logout successful');
		sessionContext.SetLoggedInStatus(false);
	};

	const LogoutFail= (errorData : string) => {
		toast.success(errorData);
		sessionContext.SetLoggedInStatus(false);
	};


	const handleLogout = () => {
		const user : ILoginPacket = ({
			Email: Email,
			Password: Password
		});
		LogoutUser(user, LogoutSuccess, LogoutFail);
	};

	const loginElements : JSX.Element = 
		<form onSubmit={handleLogin}>
			<Collection Title={'Login form'}>
				<Column>
					<label htmlFor='email'>Email:</label>
					<input type='email' id='email' value={Email} onChange={EmailChanged} required />
					<label htmlFor='fpassword'>Password:</label>
					<input type='password' id='fpassword' value={Password} onChange={PasswordChanged} required />
					<input type='submit' value={'login'}/>
					<Link to='/register'>Register</Link>
				</Column>
			</Collection>
		</form>;


	const loggedInElements : JSX.Element = 
		<Column>
			<button onClick={handleLogout}>Logout</button>
		</Column>;

	return sessionContext.IsLoggedIn ? loggedInElements : loginElements;
};
