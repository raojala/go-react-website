import React from 'react';
import ReactDOM from 'react-dom';
import { Home } from './views/home/home';

import './defaults.scss';
import { SetupDefaults } from './defaults';

SetupDefaults();

// entry point.
const RenderApp = () : void => {
	const rootElement = document.getElementById('root');
	ReactDOM.render(<Home />, rootElement);
};

RenderApp();

/*  miten kutsua erinäköisiä virheitä
	toast.warn('daadaa');
	toast.success('daadaa');
	toast.info('daadaa');
	toast.error('daadaa');
*/