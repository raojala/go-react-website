import React from 'react';

export const Content = () : JSX.Element => {
	return (
		<fieldset className='content'>
			<fieldset className='news'>
				<legend>news</legend>
				<fieldset className='article'>
					<legend>Article 1</legend>
				</fieldset>
				<fieldset className='article'>
					<legend>Article 2</legend>
				</fieldset>
				<fieldset className='article'>
					<legend>Article 1</legend>
				</fieldset>
				<fieldset className='article'>
					<legend>Article 2</legend>
				</fieldset>
				<fieldset className='article'>
					<legend>Article 1</legend>
				</fieldset>
				<fieldset className='article'>
					<legend>Article 2</legend>
				</fieldset>
			</fieldset>
			<fieldset className='aside'>
				<legend>aside</legend>
				<fieldset className='article'>
					<legend>Aside 1</legend>
				</fieldset>
				<fieldset className='article'>
					<legend>Aside 1</legend>
				</fieldset>
			</fieldset>
		</fieldset>
	);
};