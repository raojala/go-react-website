import React, { useEffect, useState } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { CheckLoginStatus } from '../../api_calls/session_api_calls';
import { SessionContext } from '../../context';
import { LoginForm } from '../../forms/login_form';
import { RegisterUserForm } from '../../forms/register_form';
import { Content } from '../content/content';
import { Footer } from '../footer/footer';
import { Header } from '../header/header';
import { Menu } from '../menu/menu';
import { ToastContainer } from 'react-toastify';

import './homestyles.scss';

export const Home = () : JSX.Element => {

	useEffect(() => {
		CheckLoginStatus(SetLoggedInStatus);
	}, []);

	const [IsLoggedIn, SetLoggedInStatus] = useState<boolean|null>(null);

	const NotLoggedIn = () => 
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<LoginForm />} />
				<Route path='/login' element={<LoginForm />} />
				<Route path='/register' element={<RegisterUserForm />} />
			</Routes>
		</BrowserRouter>;

	const LoggedIn = () => 
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<HomeView />} />
				<Route
					path='*'
					element={<Navigate to='/' />}
				/>
			</Routes>
		</BrowserRouter>;
	
	const view = IsLoggedIn == true ? LoggedIn() : NotLoggedIn();
	
	return (
		<SessionContext.Provider value={{IsLoggedIn, SetLoggedInStatus}}>
			<ToastContainer
				position="bottom-right"
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss={false}
				draggable={false}
			/>
			{IsLoggedIn == null ? <div></div> : view}
		</SessionContext.Provider>
	);
};

const HomeView = () => <div>
	<Header />
	<Menu />
	<Content />
	<Footer />
</div>;
