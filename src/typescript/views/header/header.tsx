import React from 'react';
import { LoginForm } from '../../forms/login_form';

export const Header = () : JSX.Element => {

	return (
		<fieldset>
			<h2>HEADER</h2>
			<LoginForm />
		</fieldset>
	);
};
