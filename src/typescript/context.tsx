import { createContext } from 'react';

interface test {
    IsLoggedIn: boolean|null,
    SetLoggedInStatus: (val:boolean)=>void,
}

export const SessionContext = createContext<test>({
	IsLoggedIn:null, 
	SetLoggedInStatus:(val:boolean)=>console.log(val)
});