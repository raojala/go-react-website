/**
 * Displays all components inside left to right
 */
import React from 'react';
import './row.scss';

export interface IRowProps {
    children : JSX.Element | Array<JSX.Element>,
}

export const Row = (props : IRowProps) : JSX.Element => {

	return (
		<div className='Row'>
			{props.children}
		</div>
	);
};
