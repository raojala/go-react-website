/**
 * Displays all components inside top to bottom
 */
import React from 'react';
import './column.scss';

interface IColumnProps {
    children : JSX.Element | Array<JSX.Element>,
}
 
export const Column = (props : IColumnProps) : JSX.Element => {
	return (
		<div className='Column'>
			{props.children}
		</div>
	);
};

