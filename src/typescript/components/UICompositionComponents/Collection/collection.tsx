/**
 * Displays all components inside left to right
 */
import React from 'react';

export interface ICollectionProps {
	Title: string;
	Disabled? : boolean;
    children : JSX.Element | Array<JSX.Element>,
}

/**
 * Collection wraps the children inside a fieldset
 */
export const Collection = (props : ICollectionProps) : JSX.Element => {

	return (
		<fieldset disabled={props.Disabled}>
			<legend>{props.Title}</legend>
			{props.children}
		</fieldset>
	);
};