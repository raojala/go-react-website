import axios from 'axios';

export const SetupDefaults = () : void => {
	InitAxiosDefaults();
};

// Setting up
const InitAxiosDefaults = () => {
	const csrfDiv = document.getElementById('csrf');
	axios.defaults.headers.common['X-CSRF-TOKEN'] = csrfDiv ? csrfDiv.innerHTML : '';
	axios.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8';
	axios.defaults.headers.common['Accept'] = 'application/json;charset=UTF-8';

};
