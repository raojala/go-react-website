module https-react

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/wagslane/go-password-validator v0.3.0
	golang.org/x/crypto v0.0.0-20220518034528-6f7dac969898
)
