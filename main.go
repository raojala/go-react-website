package main

import (
	"log"
	"mime"
	"net/http"
	"time"

	"https-react/src/go/dal"
	"https-react/src/go/env_wrapper"
	"https-react/src/go/log_wrapper"
	"https-react/src/go/routes"
	"https-react/src/go/session"
)

func main() {
	checkEarlyDependencies()
	RegisterMIMETypes()

	// format: "root:salasana@tcp(172.16.28.4:20001)/mydb"
	userPwd := env_wrapper.EnvSettings.MySQLUser + ":" + env_wrapper.EnvSettings.MySQLPassword
	ipPort := env_wrapper.EnvSettings.MySQLDatabaseAddress + ":" + env_wrapper.EnvSettings.MySQLPort
	sqlConnStr := userPwd + "@" + env_wrapper.EnvSettings.MySQLProtocol + "(" + ipPort + ")/" + env_wrapper.EnvSettings.MySQLDatabase
	dal.ConnectMySQL(
		sqlConnStr,
		env_wrapper.EnvSettings.MySQLMaxConnections,
		env_wrapper.EnvSettings.MySQLMaxIdleConnections,
		time.Duration(env_wrapper.EnvSettings.MySQLMaxConnectionLifetimeSeconds)*time.Second,
	)

	session.InitSessionManagement()

	apiMux := routes.SetupAPI()
	homeMux := routes.SetupWebserver(env_wrapper.EnvSettings.WebsiteStaticFilesPath, env_wrapper.EnvSettings.WebsiteBuildPath)

	indexMux := http.NewServeMux()
	indexMux.Handle("/api/", apiMux)
	indexMux.Handle("/", homeMux)

	if err := http.ListenAndServeTLS(
		env_wrapper.EnvSettings.WebsiteIPAddress+":"+env_wrapper.EnvSettings.WebsitePort,
		env_wrapper.EnvSettings.ServerCertificateCrt,
		env_wrapper.EnvSettings.ServerCertificateKey,
		indexMux,
	); err != nil {
		log_wrapper.SysLog.Error.Fatal(err)
	}
}

func checkEarlyDependencies() {

	if err := log_wrapper.NewLog(log.Ldate | log.Ltime | log.Lshortfile); err != nil {
		log.Panic("could not initialize log wrapper", err)
	}

	if err := env_wrapper.LoadSettings(); err != nil {
		log.Panic("could not load environment variables", err)
	}
}

func RegisterMIMETypes() {
	mime.AddExtensionType(".js", "text/javascript")
	mime.AddExtensionType(".css", "text/css")
}
